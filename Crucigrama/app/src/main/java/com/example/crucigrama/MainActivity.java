package com.example.crucigrama;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button Iniciar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se declara el boton con el que vamos a cambiar de activity
        Iniciar = (Button) findViewById(R.id.btniniciar);
        //Creamos el evento para cuando se de click nos mande al segundo activity
        Iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(MainActivity.this, Crucigrama.class);
                startActivity(intent);
            }
        });
    }
}