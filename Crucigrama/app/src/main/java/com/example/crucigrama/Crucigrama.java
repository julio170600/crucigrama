package com.example.crucigrama;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Crucigrama extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crucigrama);
        Button btn = (Button) findViewById(R.id.btncomprobar);

        //Se genera la funcion de dar click
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Se crean y dan valores a los txt correspondientes
                EditText txt1 = (EditText) findViewById(R.id.PalaE);
                EditText txt2 = (EditText) findViewById(R.id.PalaZ);
                EditText txt3 = (EditText) findViewById(R.id.PalaI);
                EditText txt4 = (EditText) findViewById(R.id.PalaE2);
                EditText txt5 = (EditText) findViewById(R.id.PalaY);
                EditText txt6 = (EditText) findViewById(R.id.PalaI2);
                EditText txt7 = (EditText) findViewById(R.id.PalaS);
                EditText txt8 = (EditText) findViewById(R.id.PalaI3);
                EditText txt9 = (EditText) findViewById(R.id.PalaR2);
                //Se conviertem los que tienen los campos de texto a string
                String E = txt1.getText().toString();
                String Z = txt2.getText().toString();
                String I = txt3.getText().toString();
                String E2 = txt4.getText().toString();
                String Y = txt5.getText().toString();
                String I2 = txt6.getText().toString();
                String S = txt7.getText().toString();
                String I3 = txt8.getText().toString();
                String R2 = txt9.getText().toString();

                //Se crea la condicion de que si los txt no estan completos, mande un mensaje de alerta, que debe llenar todos los campos
                if (E.equals("") || Z.equals("")||I.equals("")||E2.equals("")||Y.equals("")||I2.equals("")||S.equals("")||I3.equals("")||R2.equals("")){
                   //Se manda un mnesaje para que se completen todos los campos
                    Toast.makeText(getApplicationContext(), "Complete todo los campos", Toast.LENGTH_SHORT).show();
                }

                // Se crean las condiciones por cada txt donde se verifica si el usuario puso correctamente la letra correspondeinte marcando con verde, si no se maracra de color rojo
                if (E.equalsIgnoreCase("E")){
                    //Se cambia el color del texto
                    txt1.setTextColor(Color.GREEN);
                }
                else{
                    txt1.setTextColor(Color.RED);
                }
                if (Z.equalsIgnoreCase("Z")){
                    txt2.setTextColor(Color.GREEN);
                }
                else{
                    txt2.setTextColor(Color.RED);
                }
                if (I.equalsIgnoreCase("I")){
                    txt3.setTextColor(Color.GREEN);
                }
                else{
                    txt3.setTextColor(Color.RED);
                }
                if (E2.equalsIgnoreCase("E")){
                    txt4.setTextColor(Color.GREEN);
                }
                else{
                    txt4.setTextColor(Color.RED);
                }
                if (Y.equalsIgnoreCase("Y")){
                    txt5.setTextColor(Color.GREEN);
                }
                else{
                    txt5.setTextColor(Color.RED);
                }
                if (I2.equalsIgnoreCase("I")){
                    txt6.setTextColor(Color.GREEN);
                }
                else{
                    txt6.setTextColor(Color.RED);
                }
                if (S.equalsIgnoreCase("S")){
                    txt7.setTextColor(Color.GREEN);
                }
                else{
                    txt7.setTextColor(Color.RED);
                }
                if (I3.equalsIgnoreCase("I")){
                    txt8.setTextColor(Color.GREEN);
                }
                else{
                    txt8.setTextColor(Color.RED);
                }
                if (R2.equalsIgnoreCase("R")){
                    txt9.setTextColor(Color.GREEN);
                }
                else{
                    txt9.setTextColor(Color.RED);
                }
            }


        });

    }
}